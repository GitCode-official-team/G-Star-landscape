在2024年10月25日成功举办首届 GitCode 开源共创大会暨 G-Star 嘉年华，并成功发布 G-Star Landscape 1.0 版本后，我们收到了广大G-Star作者的积极反馈与建议。为了更好地服务于开源生态建设，我们将持续改进 Landscape，进一步提升 G-Star Landscape 的实用性与全面性。

此仓库用作G-Star landscape项目征集和不同版本的展示、下载。